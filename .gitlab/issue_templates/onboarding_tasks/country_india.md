### Day 1 - For Team Members in India only

<details>
<summary>New Team Member</summary>

1. [ ] New team member: Read through the [India Specific Benefits](https://about.gitlab.com/handbook/benefits/#specific-to-india-based-employees). This will explain what is available. If you have any questions please contact Lyra HR at +91 80 40408181 or email them at hr@lyrainfo.com. Lyra HR will also reach out to you in the first week of starting at GitLab to complete their onboarding documents for payroll (if this has not already been done during the contract signing stage).

</details>

<details>
<summary>People Experience</summary>

1. [ ] People Ops: Double check that the `Employment Status` in BambooHR has three entries:   
   * Effective Date: `Hire Date`  Employment Status: `Probationary Period`  Comment: `6-month Probationary Period until YYYY-MM-DD (6 months after hire date)` 
   * Effective Date: `6 months after Hire Date` Employment Status: `End of Probation Period` Comment: End of Probationary Period
   * Effective Date: `6 months + 1 day after Hire Date` Employment Status: `Active` Comment: No need to comment.
1. [ ] People Experience: Once the legal name in BambooHR has been checked, add a comment in the onboarding issue tagging the People Ops Analyst. State that the new team member's profile is "Ready to audit". Comment on Day 1 at the earliest.

</details>
