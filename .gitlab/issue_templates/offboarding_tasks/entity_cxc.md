### For CXC Employees and Contractors

<summary>People Ops</summary>

1. [ ] People Ops: For voluntary terms, contact the appropriate CXC representative of the last day of employment (contact details are in People Ops 1Password Vault).
