### For GitLab BV employees only

<summary>People Ops</summary>

1. [ ] People Ops: Remove former team member HRSavvy, inform co-employer payrolls.